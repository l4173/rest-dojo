package de.linovy.restdojo.domain;

import org.springframework.stereotype.Service;

@Service
public class TennisGameFactory {
    public TennisGame createNewGame(String player1Name, String player2Name) {
        final var player1 = new TennisPlayer(player1Name);
        final var player2 = new TennisPlayer(player2Name);
        return new TennisGame(player1, player2);
    }
}
