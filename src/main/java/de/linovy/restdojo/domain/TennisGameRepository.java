package de.linovy.restdojo.domain;

public interface TennisGameRepository {
    TennisGame getActiveGame();

    void save(TennisGame tennisGame);
}
