package de.linovy.restdojo.domain;

public class TennisGame {
    private final TennisPlayer tennisPlayerOne;
    private final TennisPlayer tennisPlayerTwo;

    public TennisGame(TennisPlayer tennisPlayerOne, TennisPlayer tennisPlayerTwo) {
        this.tennisPlayerOne = tennisPlayerOne;
        this.tennisPlayerTwo = tennisPlayerTwo;
    }

    public boolean isGameOver() {
        return false;
    }

    public String getScore() {
        return tennisPlayerOne.getScore() + ":" + tennisPlayerTwo.getScore();
    }

    public void submitScoreForPlayer(String name) {
        if (name.equals(tennisPlayerOne.getName())) {
            tennisPlayerOne.increaseScore();
        } else {
            tennisPlayerTwo.increaseScore();
        }
    }
}
