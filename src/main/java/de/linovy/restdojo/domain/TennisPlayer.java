package de.linovy.restdojo.domain;

public class TennisPlayer {
    private int score = 0;
    private String name;

    public TennisPlayer(String playerName) {
        this.name = playerName;
    }

    public void increaseScore() {
        this.score = this.score + 1;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }
}
