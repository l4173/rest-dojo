package de.linovy.restdojo.application;

import de.linovy.restdojo.domain.TennisGameFactory;
import de.linovy.restdojo.domain.TennisGameRepository;
import org.springframework.stereotype.Service;

@Service
public class StartGame {
    private final TennisGameRepository tennisGameRepository;

    public StartGame(TennisGameRepository tennisGameRepository) {
        this.tennisGameRepository = tennisGameRepository;
    }

    public void start(String player1Name, String player2Name) {
        final var tennisGameFactory = new TennisGameFactory();
        final var tennisGame = tennisGameFactory.createNewGame(player1Name, player2Name);
        tennisGameRepository.save(tennisGame);
    }
}
