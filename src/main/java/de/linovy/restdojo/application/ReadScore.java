package de.linovy.restdojo.application;

import de.linovy.restdojo.domain.TennisGameRepository;
import org.springframework.stereotype.Service;

@Service
public class ReadScore {
    private final TennisGameRepository tennisGameRepository;

    public ReadScore(TennisGameRepository tennisGameRepository) {
        this.tennisGameRepository = tennisGameRepository;
    }

    public String read() {
        final var activeGame = tennisGameRepository.getActiveGame();
        return activeGame.getScore();
    }
}
