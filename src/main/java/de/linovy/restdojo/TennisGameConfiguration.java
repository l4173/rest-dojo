package de.linovy.restdojo;

import de.linovy.restdojo.domain.TennisGameRepository;
import de.linovy.restdojo.infrastructure.InMemoryTennisGameRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TennisGameConfiguration {
    @Bean
    public TennisGameRepository tennisGameRepository() {
        return new InMemoryTennisGameRepository();
    }
}
