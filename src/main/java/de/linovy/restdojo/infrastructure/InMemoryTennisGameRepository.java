package de.linovy.restdojo.infrastructure;

import de.linovy.restdojo.domain.TennisGame;
import de.linovy.restdojo.domain.TennisGameRepository;

public class InMemoryTennisGameRepository implements TennisGameRepository {
    private static TennisGame activeGame;

    @Override
    public TennisGame getActiveGame() {
        return activeGame;
    }

    @Override
    public void save(TennisGame tennisGame) {
        activeGame = tennisGame;
    }
}
