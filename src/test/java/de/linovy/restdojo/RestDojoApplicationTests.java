package de.linovy.restdojo;

import de.linovy.restdojo.api.GameConfigurationDto;
import de.linovy.restdojo.api.TennisGameController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class RestDojoApplicationTests {
    final TennisGameController tennisGameController;

    @Autowired
    RestDojoApplicationTests(TennisGameController restDojoController) {
        this.tennisGameController = restDojoController;
    }

    @Test
    void testShouldVerifyStartingScoreToBe00() {
        // arrange
        final var gameConfiguration = new GameConfigurationDto();
        gameConfiguration.setPlayerOneName("test");
        gameConfiguration.setPlayerTwoName("test2");

        // act
        tennisGameController.start(gameConfiguration);

        // assert
        assertEquals("0:0", tennisGameController.score().getBody());
    }

    @Test
    void testShouldVerifyScoreToBeOneZero() {
        // arrange
        final var gameConfiguration = new GameConfigurationDto();
        gameConfiguration.setPlayerOneName("test");
        gameConfiguration.setPlayerTwoName("test2");

        // act
        tennisGameController.start(gameConfiguration);
        tennisGameController.playerScores("test2");

        // assert
        assertEquals("0:1", tennisGameController.score().getBody());
    }
}
